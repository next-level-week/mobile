import React, { useState, useEffect } from 'react'
import { Feather as Icon } from '@expo/vector-icons'
import { View, Image, ImageBackground, Text, TextInput, KeyboardAvoidingView, Platform, Keyboard, TouchableWithoutFeedback } from 'react-native'
import { RectButton } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'

import styles from './styles'

const logo = require('../../assets/logo.png')
const bgImage = require('../../assets/home-background.png')

const Home = () => {

  const [uf, setUf] = useState('')
  const [city, setCity] = useState('')

  const navigation = useNavigation()

  function handleNavigateToPoints() {
    navigation.navigate('Points', {uf, city})
  }

  return(
    <KeyboardAvoidingView 
      style={{ flex: 1 }} 
      behavior={Platform.OS == "ios" ? "padding" : "height"}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      
        <ImageBackground 
          source={bgImage} 
          style={styles.container}
          imageStyle={styles.bgImage}
        >
          <View style={styles.main}>
            <Image source={logo} />
            <View>
              <Text style={styles.title}>Seu marketplace de coleta de redíduos</Text>
              <Text style={styles.description}>Ajudamos pessoas a encontrarem pontos de coleta de forma eficiente</Text>
            </View>
          </View>

          {/* TODO: Utilizar a API do IBGE para trazer os estados e cidades */}
          <View style={styles.footer}>
            <TextInput
              style={styles.input}
              placeholder="Digite a UF"
              value={uf}
              maxLength={2}
              autoCapitalize="characters"
              autoCorrect={false}
              onChangeText={setUf}
            />

            <TextInput
              style={styles.input}
              placeholder="Digite a Cidade"
              value={city}
              autoCorrect={false}
              onChangeText={setCity}
            />

            <RectButton style={styles.button} onPress={handleNavigateToPoints}>
              <View style={styles.buttonIcon}>
                <Icon name="arrow-right" color="#FFF" size={24} />
              </View>
              <Text style={styles.buttonText}>
                Entrar
              </Text>
            </RectButton>
          </View>

        </ImageBackground>
      </TouchableWithoutFeedback>

    </KeyboardAvoidingView>
  )

}

export default Home