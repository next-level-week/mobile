import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  mainBg: {
    backgroundColor: '#f0f0f5'
  }
})
